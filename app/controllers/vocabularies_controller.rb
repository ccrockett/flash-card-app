class VocabulariesController < ApplicationController

  # GET /vocabularies
  # GET /vocabularies.json
  def index
    @vocabularies = Vocabulary.select(:id, :word, :definition).all
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vocabulary
      @vocabulary = Vocabulary.find(params[:id])
    end
end
