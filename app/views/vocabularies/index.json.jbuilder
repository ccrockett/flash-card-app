json.array! @vocabularies do |vocab|
  json.id vocab.id
  json.word vocab.word
  json.definition vocab.definition
end