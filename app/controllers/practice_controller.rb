class PracticeController < ApplicationController
  def index
    @vocabularies = Vocabulary.select(:id, :word, :definition).all
  end
end
