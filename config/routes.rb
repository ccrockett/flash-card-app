Rails.application.routes.draw do
  root 'practice#index'

  resources :practice, only: [:index]
  resources :vocabularies, only: [:index]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
