// Run this example by adding <%= javascript_pack_tag 'hello_react' %> to the head of your layout file,
// like app/views/layouts/application.html.erb. All it does is render <div>Hello React</div> at the bottom
// of the page.

import React from 'react'
import ReactDOM from 'react-dom'
import {
  Container,
  Header,
  Segment,
  Form,
  Radio,
  Message,
  Menu,
  Label,
  Input,
  Grid
} from 'semantic-ui-react'
import '../../../dist/semantic.min.css'

class Study extends React.Component {
  constructor(props) {
    const numberOfChoices = 4
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
      selectedItem: 0,
      wordOrDef: null,
      numOfChoices: numberOfChoices,
      selectedValue: -1,
      message: {
        success: false,
        display: false
      },
      count: 1,
      answered: {
        correct: 0,
        incorrect: 0
      }
    };
  }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  getRandomAnswers(numOfAnwsers, correctAnswer, items) {
    let randomAnswers = [];
    let addAnswers = 0;
    let insertedCorrectAnswer = false;
    let correctIndex = -1;
    console.debug("Correct answer is " + correctAnswer);
    while (randomAnswers.length < numOfAnwsers) {
      let correctItem = this.getRandomInt(2);
      if (insertedCorrectAnswer === false && ((addAnswers + 1 >= numOfAnwsers) || correctItem === 1)) {
        randomAnswers.push(items[correctAnswer]);
        console.debug("Inserted Correct Answer");
        insertedCorrectAnswer = true;
      } else {
        let randomItem = this.getRandomInt(items.length);
        if (randomItem != correctAnswer) {
          randomAnswers.push(items[randomItem]);
        }
      }
    }
    return randomAnswers;
  }

  checkSelection() {
    let answered = this.state.answered;
    if (this.state.selectedItem.id === this.state.selectedValue) {
      let results = this.state.items;
      let selectedItem = this.getRandomInt(results.length + 1)
      let wordOrDef = this.getRandomInt(2)
      let answers = this.getRandomAnswers(this.state.numOfChoices, selectedItem, results);
      let count = this.state.count + 1;
      answered.correct = this.state.answered.correct + 1;
      this.setState({
        selectedItem: results[selectedItem],
        wordOrDef: wordOrDef,
        answers: answers,
        count: count,
        answered: answered
      });
    } else {
      answered.incorrect = this.state.answered.incorrect + 1;
      this.setState({
        message: {
          success: false,
          display: true
        },
        answered: answered
      })
    }
  }

  handleSubmit = () => {
    this.checkSelection();
  }

  componentDidMount() {
    let host = location.protocol + '//' + location.host;
    fetch(host + '/vocabularies.json')
      .then(res => res.json())
      .then(
        (results) => {
          let selectedItem = this.getRandomInt((results.length + 1))
          let wordOrDef = this.getRandomInt(2)
          let answers = this.getRandomAnswers(this.state.numOfChoices, selectedItem, results);
          this.setState({
            isLoaded: true,
            items: results,
            correctItemId: results[selectedItem].id,
            selectedItem: results[selectedItem],
            wordOrDef: wordOrDef,
            answers: answers,
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error: error
          });
        }
      )
  }

  handleChange = (e, { value }) => this.setState({
    selectedValue: value,
    message: {
      success: false,
      display: false
    }
  });

  render() {
    const style = {
      h1: {
        marginTop: '3em',
      },
      h2: {
        margin: '4em 0em 2em',
      },
      h3: {
        marginTop: '2em',
        padding: '2em 0em',
      },
      last: {
        marginBottom: '300px',
      },
    }
    let i = this.state;
    if (this.state.isLoaded) {
      return (
        <Grid centered>
          <Grid.Column>
            <Header as='h1' textAlign='center' style={style.h1} content='Practice' />
          </Grid.Column>
          <Grid.Row centered columns={2}>
            <Grid.Column width={4} >
              <Stats answered={this.state.answered} />
            </Grid.Column>
            <Grid.Column>
              <Container>
                <Form onSubmit={(i) => this.handleSubmit(i)}>
                  <Question count={this.state.count} type={this.state.wordOrDef} vocab={this.state.selectedItem} />
                  <AnswerList onSelectionChange={this.handleChange} answers={this.state.answers}
                    selectedValue={this.state.selectedValue} wordOrDef={this.state.wordOrDef} />
                  <Message negative={!this.state.message.success} hidden={!this.state.message.display}>
                    {this.state.message.success ? 'Correct Answer' : 'Incorrect Answer. Try Again'}
                  </Message>
                  <Form.Button>Submit</Form.Button>
                </Form>
              </Container>
            </Grid.Column>

            <Grid.Column></Grid.Column>
          </Grid.Row>

        </Grid>
      )
    } else {
      return (<div>Loading .... </div>)
    }
  }
}

class Question extends React.Component {
  render() {
    // return <div>Question</div>
    if (this.props.type == 0) {
      return (<div><h1>Question {this.props.count}:</h1><h2>Select the definition of '{this.props.vocab.word}' ?</h2></div>)
    } else {
      return (<div><h1>Question {this.props.count}:</h1><h2>Select the word that satisfies the definition:</h2>
        <code>{this.props.vocab.definition}</code></div>)
    }
  }
}

class AnswerList extends React.Component {
  render() {
    const answerItems = this.props.answers.map((answer) =>
      <Segment key={answer.id} >
        <Answers
          type={this.props.wordOrDef}
          vocab={answer}
          onChange={this.props.onSelectionChange} selectedValue={this.props.selectedValue} /></Segment>
    )
    return (<Segment.Group>{answerItems}</Segment.Group>);
  }
}
class Answers extends React.Component {
  render() {
    return (<div><Form.Field
      control={Radio}
      name='answer'
      value={this.props.vocab.id}
      checked={this.props.selectedValue === this.props.vocab.id}
      onChange={this.props.onChange}
      label={this.props.type == 0 ? this.props.vocab.definition : this.props.vocab.word}
    /></div>)
  }
}

class Stats extends React.Component {
  render() {
    return (
      <Menu vertical>
        <Menu.Item name='Correct'>
          <Label color='teal'>{this.props.answered.correct}</Label>
          Correct
        </Menu.Item>
        <Menu.Item name='Incorrect'>
          <Label color='red'>{this.props.answered.incorrect}</Label>
          Incorrect
        </Menu.Item>
      </Menu>
    )
  }
}
document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(<Study />,
    document.body.getElementsByTagName("Study")[0],
  );
})
