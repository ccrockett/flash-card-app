# README

Simple Rails App to study for tests that require vocabulary.  
The app will randomly choose either  
  
choose the definition and the user has to pick the correct word
-or-
it will show the word and the user has to pick the correct definition

See a demo at https://bcba.crockettonline.com

Tech Stack:

* Rails Backend
* REACT frontend
* Semantic UI

